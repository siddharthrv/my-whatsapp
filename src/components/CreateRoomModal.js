import React, { useState, useEffect } from "react";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import db from '../configs/Firebase'

export default function CreateRoomModal({openModal, handleCloseModal}) {
  const [open, setOpen] = useState(openModal);
  const [roomName, setRoomName] = useState("");

  const handleCreateRoom = () => {
    if (roomName) {
        db.collection("rooms").add({
          name: roomName,
        });
      }
    setOpen(false);
    handleCloseModal()
  };

  useEffect(() => {
    setOpen(openModal)
  }, [openModal]);

  const handleClose = () => {
    setOpen(false);
    handleCloseModal()
  };

  return (
    <div>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Create Room</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Please enter the room name to create
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="room name"
            type="text"
            value={roomName}
            fullWidth
            onChange={(e) => {
                setRoomName(e.target.value);
              }}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleCreateRoom} color="primary">
            Create
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}