import { Button } from "@material-ui/core";
import React from "react";
import { auth, provider } from "../configs/Firebase";
import "../css/Login.css";
import { actionTypes } from "../reducer";
import { useStateValue } from "../StateProvider";
import sid from '../images/sid.png'

function Login() {
  const [{}, dispatch] = useStateValue()

  const signIn = ()=>{
    auth.signInWithPopup(provider).then(result=> {
        dispatch({
            type: actionTypes.SET_USER,
            user: result.user
        })
    }).catch(e=>{
        alert(e.message)
    })
  }

  return (
    <div className="login">
      <div className="login__container">
        <img src={sid}></img>
        <div className="login__text">
            <h1>Sign in to Sid Chat</h1>
        </div>
        <Button onClick={signIn}>
            Sign in with google
        </Button>
      </div>
    </div>
  );
}

export default Login;
