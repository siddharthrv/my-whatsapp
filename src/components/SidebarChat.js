import { Avatar } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import db from "../configs/Firebase";
import "../css/SidebarChat.css";
import {Link} from 'react-router-dom';
import CreateRoomModal from './CreateRoomModal';
function SidebarChat({ addNewChat, id, name }) {
  const [seed, setSeed] = useState("");
  const [openModal, setOpenModal] = useState(false);
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    if(id){
        db.collection("rooms")
        .doc(id)
        .collection("messages")
        .orderBy("timestamp", "desc")
        .onSnapshot((snapShot) => {
          setMessages(snapShot.docs.map((doc) => doc.data()));
        });
    }
  }, [id]);

  useEffect(() => {
    setSeed(Math.floor(Math.random() * 5000));
  }, []);

  const createChat = () => {
    setOpenModal(true)
  };

  const handleCloseModal = () => {
    setOpenModal(false)
  };

  return !addNewChat ? (
    <Link to={`/rooms/${id}`}>
      <div className="sidebarChat">
        <Avatar src={`https://avatars.dicebear.com/api/human/${seed}.svg`} />
        <div className="sidebarChat__info">
          <h2>{name}</h2>
          <p>{messages[0]?.message}</p>
        </div>
      </div>
    </Link>
  ) : (
    <div>
        <div onClick={()=>createChat()} className="sidebarChat">
        <h2>Add new Chat</h2>
        </div>
        <CreateRoomModal openModal={openModal} handleCloseModal={handleCloseModal}/>
    </div>
  );
}

export default SidebarChat;
