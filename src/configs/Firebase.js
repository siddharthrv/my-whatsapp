import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyB6dSv7pT5mtKSrp4lHxdPy28OcQdyjtV4",
    authDomain: "whatsapp-clone-7e820.firebaseapp.com",
    projectId: "whatsapp-clone-7e820",
    storageBucket: "whatsapp-clone-7e820.appspot.com",
    messagingSenderId: "911783601982",
    appId: "1:911783601982:web:0b507862daafb112096e6f",
    measurementId: "G-QB42VKN2S3"
  };

  const firebaseapp = firebase.initializeApp(firebaseConfig)

  const db = firebaseapp.firestore()

  const auth = firebase.auth()

  const provider = new firebase.auth.GoogleAuthProvider()

  export {auth, provider}

  export default db